<?php

namespace Drupal\hms_appointments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\taxonomy\Entity\Term;

/**
 * Class AppointmentsController.
 */
class AppointmentsController extends ControllerBase {

  /**
   * Addappointment.
   *
   * @return string
   *   Return Hello string.
   */
  public function addAppointment() {
    $values = array('type' => 'appointment');
    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->create($values);

    $form = \Drupal::entityTypeManager()
      ->getFormObject('node', 'default')
      ->setEntity($node);
    return \Drupal::formBuilder()->getForm($form);
  }

  /**
   * editAppointment.
   *
   * @return string
   */
  public function editAppointment($appointment_id) {
    $entity = Node::load($appointment_id);
    $appointment_form = \Drupal::service('entity.form_builder')->getForm($entity);
    return $appointment_form;
  }

  /**
   * deleteAppointment.
   *
   * @return string
   */
  public function deleteAppointment($appointment_id) {
    $output =[];
    $output['#markup'] = '<div class="doctor-delete-confirm">
    <h1>Are you sure, you want to delete appointment !</h1>
    <div class="confirm-button"><a href="/appointment/'.$appointment_id.'/delete/confirm" class="add-view-user-btn">Confirm</a></div>
    <div class="confirm-button"><a href="/appointments" class="add-view-user-btn">Cancel</a></div>
    </div>';
    return $output;
  }

  /**
   * confirmDeleteAppointment.
   *
   * @return string
   */
  public function confirmDeleteAppointment($appointment_id) {
    global $base_url;
    $entity = Node::load($appointment_id);
    $entity->delete();
    drupal_set_message(t('Selected appointment has been deleted successfully.'));
    $response = new RedirectResponse($base_url . '/appointments');
    $response->send();
  }

  /**
   * viewAppointment.
   *
   * @return string
   */
  public function viewAppointment($appointment_id) {
    $appointment = Node::load($appointment_id);
    $patient_id = $appointment->field_patient->target_id;
    $account = User::load($patient_id);
    $name = $account->field_first_name->value.' '.$account->field_last_name->value;
    $doctor_id = $appointment->field_doctor->target_id;
    $accounts = User::load($doctor_id);
    $doctor_name = $accounts->field_first_name->value.' '.$accounts->field_last_name->value;
    $department_id = $appointment->field_department->target_id;
    $department = Term::load($department_id);
    $appointment_time = Date('d-m-Y h:i',$appointment->field_time->value);
    $problem = ucfirst($appointment->field_problem->value);
    $output['#markup'] = 
    '<div class="row no-mp">
        <div class="col-md-12">
            <div class="table-wrapper">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <td><strong>Patient ID</strong></td>
                            <td>'.$patient_id.'</td>
                        </tr>
                        <tr>
                            <td><strong>Patient Name</strong></td>
                            <td>'.$name.'</td>
                        </tr>
                        <tr>
                            <td><strong>Department</strong></td>
                            <td>'.ucfirst($department->getName()).'</td>
                        </tr>
                        <tr>
                            <td><strong>Doctor Name</strong></td>
                            <td>'.Ucwords($doctor_name).'</td>
                        </tr>
                        <tr>
                            <td><strong>Appointment Time</strong> </td>
                            <td>'.$appointment_time.'</td>
                        </tr>
                        <tr>
                            <td><strong>Problem</strong></td>
                            <td>'.$problem.'</td>
                        </tr>
                    </tbody>
                </table>
                <div class="view-action-btn-wrapper">
                   <a href="/appointment/'.$appointment_id.'/edit" class="add-view-user-btn doctor-btn">Edit Appointment</a>
                    <a href="/appointment/'.$appointment_id.'/delete" class="add-view-user-btn add-view-user-btn-red doctor-btn">Delete Appointment</a>
                </div>
            </div>
        </div>
    </div>';
    return $output;
  }

}
