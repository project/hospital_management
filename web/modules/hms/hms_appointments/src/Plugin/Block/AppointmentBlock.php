<?php

namespace Drupal\hms_appointments\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'AppointmentBlock' block.
 *
 * @Block(
 *  id = "appointment_block",
 *  admin_label = @Translation("Appointment Block"),
 * )
 */
class AppointmentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    global $base_url;
    $query = \Drupal::entityQuery('node');
		$query->condition('status', 1);
		$query->condition('type', 'appointment');
		$appointment_ids = $query->execute();
    $build = [];
    $build['#theme'] = 'doctor_block';
    $build['doctor_block']['#markup'] = '<div class="col-md-12 col-sm-12 col-xs-12 appointment-padding">
        <div class="info-box">
            <span class="info-box-icon"><img src="'.$base_url.'/modules/hms/hms_appointments/images/appointments.png" width="60"></span>
            <div class="info-box-content">
                <span class="info-box-text">Appointments</span>
                <span class="info-box-number">'.count($appointment_ids).'</span>
            </div>
        </div>
    </div>';

    return $build;
  }

}
