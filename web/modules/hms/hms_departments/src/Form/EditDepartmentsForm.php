<?php

namespace Drupal\hms_departments\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class EditDepartmentsForm.
 */
class EditDepartmentsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'edit_departments_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $department_id = \Drupal::routeMatch()->getParameter('department_id');
    $term = Term::load($department_id);
    $form['department_id'] = [
      '#type' => 'hidden',
      '#value' => $department_id,
    ];
    $form['department_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Department Name'),
      '#default_value' => $term->getName(),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['department_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#maxlength' => 64,
      '#default_value' => $term->getDescription(),
      '#size' => 64,
      '#weight' => '1',
    ];
    $form['dep_status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Status'),
      '#options' => [1=>'Active',0=>'Inactive'],
      '#default_value' => $term->get('status')->value,
      '#weight' => '2',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '4',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $department_id = $form_state->getValue('department_id');
    $name = $form_state->getValue('department_name');
    $description = $form_state->getValue('department_desc');
    $status = $form_state->getValue('dep_status');
    $term = Term::load($department_id);
    $term->name = $name;
    $term->description = $description;
    $term->status = $status;
    $term->save();
    $response = new RedirectResponse($base_url . '/departments');
    $response->send();
  }

}
