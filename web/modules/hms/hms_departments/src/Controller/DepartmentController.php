<?php

namespace Drupal\hms_departments\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DepartmentController.
 */
class DepartmentController extends ControllerBase {

  /**
   * deleteDepartment.
   *
   * @return string
   */
  public function deleteDepartment($department_id) {
    $output =[];
    $output['#markup'] = '<div class="doctor-delete-confirm">
    <h1>Are you sure, you want to delete !</h1>
    <div class="confirm-button"><a href="/department/'.$department_id.'/delete/confirm" class="add-view-user-btn">Confirm</a></div>
    <div class="confirm-button"><a href="/departments" class="add-view-user-btn">Cancel</a></div>
    </div>';
    return $output;
  }

  /**
   * confirmDeleteDepartment.
   *
   * @return string
   */
  public function confirmDeleteDepartment($department_id) {
    global $base_url;
    $entity = Term::load($department_id);
    $entity->delete();
    drupal_set_message(t('Selected department has been deleted successfully.'));
    $response = new RedirectResponse($base_url . '/departments');
    $response->send();
  }

}
