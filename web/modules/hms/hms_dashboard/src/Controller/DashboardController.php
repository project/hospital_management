<?php

namespace Drupal\hms_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DashboardController.
 */
class DashboardController extends ControllerBase {

  /**
   * Dashboard.
   *
   * @return string
   *   Return Hello string.
   */
  public function dashboard() {
    return [
      '#type' => 'markup',
      '#markup' => ''
    ];
  }

}
