<?php

namespace Drupal\hms_patients\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'PatientsBlock' block.
 *
 * @Block(
 *  id = "patients_block",
 *  admin_label = @Translation("Patients Block"),
 * )
 */
class PatientsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $patient_ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', 'patient')
      ->execute();
    global $base_url;  
    $build = [];
    $build['#theme'] = 'patients_block';
     $build['patients_block']['#markup'] = '<div class="col-md-12 col-sm-12 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon"><img src="'.$base_url.'/modules/hms/hms_patients/images/patient.png" width="60"></span>
            <div class="info-box-content">
                <span class="info-box-text">Patients</span>
                <span class="info-box-number">'.count($patient_ids).'</span>
            </div>
        </div>
    </div>';

    return $build;
  }

}
