<?php

namespace Drupal\hms_patients\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'PatientChartBlock' block.
 *
 * @Block(
 *  id = "patient_chart_block",
 *  admin_label = @Translation("Patient Chart Block"),
 * )
 */
class PatientChartBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['data']= [
      '#type' => 'markup',
      '#markup' => $this->t('<div id="curve_chart_not_found" style="display:none;">Data not available</div><div id="patient_chart" class="containter patient-chart" style="width: 100%; height:257px">Loading...</div>'),
    ];
    $build['#attached']['library'][] = 'hms_patients/hms_patients_chart_js';
    return $build;
  }

}
