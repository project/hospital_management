<?php

namespace Drupal\hms_patients\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\taxonomy\Entity\Term;

/**
 * Class PatientsController.
 */
class PatientsController extends ControllerBase {

  /**
   * Addpatient.
   *
   * @return string
   *   Return Hello string.
   */
  public function addPatient() {
    $entity = User::create();
    $user_form = \Drupal::service('entity.form_builder')->getForm($entity, 'patient');
    return $user_form;
  }

  /**
   * editPatient.
   *
   * @return string
   */
  public function editPatient($patient_id) {
    $entity = User::load($patient_id);
    $user_form = \Drupal::service('entity.form_builder')->getForm($entity, 'patient');
    return $user_form;
  }

  /**
   * deletePatient.
   *
   * @return string
   */
  public function deletePatient($patient_id) {
    $output =[];
    $output['#markup'] = '<div class="doctor-delete-confirm">
    <h1>Are you sure, you want to delete account !</h1>
    <div class="confirm-button"><a href="/patient/'.$patient_id.'/delete/confirm" class="add-view-user-btn">Confirm</a></div>
    <div class="confirm-button"><a href="/patients" class="add-view-user-btn">Cancel</a></div>
    </div>';
    return $output;
  }

  /**
   * confirmDeletePatient.
   *
   * @return string
   */
  public function confirmDeletePatient($patient_id) {
    global $base_url;
    $entity = User::load($patient_id);
    $entity->delete();
    drupal_set_message(t('Selected patient has been deleted successfully.'));
    $response = new RedirectResponse($base_url . '/patients');
    $response->send();
  }

   /**
   * viewPatient.
   *
   * @return string
   */
  public function viewPatient($patient_id) {
    $account = User::load($patient_id);
    $name = $account->field_first_name->value.' '.$account->field_last_name->value;
    $phone = $account->field_phone_no->value;
    $gender_id = $account->field_gender->target_id;
    $gender = Term::load($gender_id);
    $admit_date = $account->field_admit_date->value;
    $address_array = $account->field_address->getValue()[0];
    $address = ucwords($address_array["address_line1"].', '.$address_array["locality"].', '.$address_array["administrative_area"].', '.$address_array["country_code"].' - '.$address_array["postal_code"]);
    $description = ucfirst($account->field_description->value);
    $output['#markup'] = 
    '<div class="row no-mp">
        <div class="col-md-12">
            <div class="table-wrapper">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <td><strong>Username</strong></td>
                            <td>'.$account->getUsername().'</td>
                        </tr>
                        <tr>
                            <td><strong>Full Name</strong></td>
                            <td>'.$name.'</td>
                        </tr>
                        <tr>
                            <td><strong>Email</strong></td>
                            <td>'.ucfirst($account->getEmail()).'</td>
                        </tr>
                        <tr>
                            <td><strong>Gender</strong></td>
                            <td>'.Ucwords($gender->getName()).'</td>
                        </tr>
                        <tr>
                            <td><strong>Address</strong></td>
                            <td>'.$address.'</td>
                        </tr>
                        <tr>
                            <td><strong>Phone</strong> </td>
                            <td>'.$phone.'</td>
                        </tr>
                        <tr>
                            <td><strong>Admit Date</strong> </td>
                            <td>'.$admit_date.'</td>
                        </tr>
                        <tr>
                            <td><strong>Description</strong></td>
                            <td>'.$description.'</td>
                        </tr>
                    </tbody>
                </table>
                <div class="view-action-btn-wrapper">
                   <a href="/patient/'.$patient_id.'/edit" class="add-view-user-btn doctor-btn">Edit Patient</a>
                    <a href="/patient/'.$patient_id.'/delete" class="add-view-user-btn add-view-user-btn-red doctor-btn">Delete Patient</a>
                </div>
            </div>
        </div>
    </div>';
    return $output;
  }

}
