(function($) {
  Drupal.behaviors.chart_progress = {
    attach: function (context, settings) {
     google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

      function drawBasic() {
        var data = google.visualization.arrayToDataTable([
           ['Element', 'Patients', { role: 'style' }],
           ['JAN', 80, '#46a28b'], 
           ['FEB', 10, '#d3c620'],
           ['MAR', 35, '#46a28b'],
           ['APR', 94, '#d3c620' ],
           ['MAY', 67, '#46a28b'], 
           ['JUN', 17, '#d3c620'],
           ['JUL', 28, '#46a28b'],
           ['AUG', 55, '#d3c620' ],
           ['SEP', 74, '#46a28b'], 
           ['OCT', 2, '#d3c620'],
           ['NOV', 48, '#46a28b'],
           ['DEC', 23, '#d3c620' ],
        ]);

        var options = {
          title: '',
          legend: 'none',
          hAxis: {
            title: '',
            textStyle : {
              fontSize: 9
            }
          },
          backgroundColor: {
            fill:'#f8f8f8'     
          },
          vAxis: {
            title: 'No. of Patients'
          }
        };

        var chart = new google.visualization.ColumnChart(
          document.getElementById('patient_chart'));

        chart.draw(data, options);
      }
    }
  };
})(jQuery);