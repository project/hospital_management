<?php

namespace Drupal\hms_doctors\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DoctorBlock' block.
 *
 * @Block(
 *  id = "doctor_block",
 *  admin_label = @Translation("Doctor Block"),
 * )
 */
class DoctorBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
  	global $base_url;
    $doctor_ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('roles', 'doctor')
      ->execute();
    $build = [];
    $build['#theme'] = 'doctor_block';
    $build['doctor_block']['#markup'] = '<div class="col-md-12 col-sm-12 col-xs-12 doctor-padding">
        <div class="info-box">
            <span class="info-box-icon"><img src="'.$base_url.'/modules/hms/hms_doctors/images/doctor.png" width="60"></span>
            <div class="info-box-content">
                <span class="info-box-text">Doctors</span>
                <span class="info-box-number">'.count($doctor_ids).'</span>
            </div>
        </div>
    </div>';

    return $build;
  }

}
