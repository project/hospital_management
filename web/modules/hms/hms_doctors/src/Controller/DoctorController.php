<?php

namespace Drupal\hms_doctors\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\taxonomy\Entity\Term;

/**
 * Class DoctorController.
 */
class DoctorController extends ControllerBase {

  /**
   * Adddoctor.
   *
   * @return string
   */
  public function addDoctor() {
    $entity = User::create();
    $user_form = \Drupal::service('entity.form_builder')->getForm($entity, 'doctor');
    return $user_form;
  }

  /**
   * Editdoctor.
   *
   * @return string
   */
  public function editDoctor($doctor_id) {
    $entity = User::load($doctor_id);
    $user_form = \Drupal::service('entity.form_builder')->getForm($entity, 'doctor');
    return $user_form;
  }

  /**
   * Deletedoctor.
   *
   * @return string
   */
  public function deleteDoctor($doctor_id) {
    $output =[];
    $output['#markup'] = '<div class="doctor-delete-confirm">
    <h1>Are you sure, you want to delete account !</h1>
    <div class="confirm-button"><a href="/doctor/'.$doctor_id.'/delete/confirm" class="add-view-user-btn">Confirm</a></div>
    <div class="confirm-button"><a href="/doctors" class="add-view-user-btn">Cancel</a></div>
    </div>';
    return $output;
  }

  /**
   * ConfirmDeletedoctor.
   *
   * @return string
   */
  public function confirmDeleteDoctor($doctor_id) {
    global $base_url;
    $entity = User::load($doctor_id);
    $entity->delete();
    drupal_set_message(t('Selected doctor has been deleted successfully.'));
    $response = new RedirectResponse($base_url . '/doctors');
    $response->send();
  }

  /**
   * viewDoctor.
   *
   * @return string
   */
  public function viewDoctor($doctor_id) {
    $account = User::load($doctor_id);
    $name = $account->field_first_name->value.' '.$account->field_last_name->value;
    $speciality_id = $account->field_speciality->target_id;
    $speciality = Term::load($speciality_id);
    $phone = $account->field_phone_no->value;
    $gender_id = $account->field_gender->target_id;
    $gender = Term::load($gender_id);
    $dob = $account->field_dob->value;
    $address_array = $account->field_address->getValue()[0];
    $address = ucwords($address_array["address_line1"].', '.$address_array["locality"].', '.$address_array["administrative_area"].', '.$address_array["country_code"].' - '.$address_array["postal_code"]);
    $description = ucfirst($account->field_description->value);
    if(!empty($account->get('user_picture')->entity)){
      $picture = $account->get('user_picture')->entity->url();
    } else {
      if($gender->getName() == 'Female'){
        $picture = '/modules/hms/hms_doctors/images/female-dr.png';
      } else {
        $picture = '/modules/hms/hms_doctors/images/doctor.jpg';
      }
    }
    $output['#markup'] = 
    '<div class="row no-mp">
        <div class="col-md-3">
            <div class="card mb-4">
                <img class="card-img-top" src="'.$picture.'" alt="Doctor >
                <div class="card-body">
                    <h4 class="card-title">Dr '.$name.'</h4>
                    <p class="card-text">'.ucwords($speciality->getName()).'</p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="table-wrapper">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <td><strong>Username</strong></td>
                            <td>'.$account->getUsername().'</td>
                        </tr>
                        <tr>
                            <td><strong>Description</strong></td>
                            <td>'.$description.'</td>
                        </tr>
                        <tr>
                            <td><strong>Gender</strong></td>
                            <td>'.Ucwords($gender->getName()).'</td>
                        </tr>
                        <tr>
                            <td><strong>Address</strong></td>
                            <td>'.$address.'</td>
                        </tr>
                        <tr>
                            <td><strong>Phone</strong> </td>
                            <td>'.$phone.'</td>
                        </tr>
                        <tr>
                            <td><strong>Date Of Birth</strong> </td>
                            <td>'.$dob.'</td>
                        </tr>
                        <tr>
                            <td><strong>Email</strong></td>
                            <td>'.ucfirst($account->getEmail()).'</td>
                        </tr>
                    </tbody>
                </table>
                <div class="view-action-btn-wrapper">
                   <a href="/doctor/'.$doctor_id.'/edit" class="add-view-user-btn doctor-btn">Edit Doctor</a>
                    <a href="/doctor/'.$doctor_id.'/delete" class="add-view-user-btn add-view-user-btn-red doctor-btn">Delete Doctor</a>
                </div>
            </div>
        </div>
    </div>';
    return $output;
  }

}
